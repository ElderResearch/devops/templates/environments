#!/bin/bash
set -e
set -o pipefail

# "Automated Mac Setup Script"

# copy dotfiles to appropriate places (and create any necessary directories)
cat dotfiles/.hushlogin >> ~/.hushlogin
cat dotfiles/.vimrc >> ~/.vimrc
cat dotfiles/.bash_profile >> ~/.bash_profile
cat dotfiles/.inputrc >> ~/.inputrc
cat dotfiles/.brewfile >> ~/.brewfile
mkdir -p ~/.ssh/ && cat dotfiles/.ssh_config >> ~/.ssh/config
mkdir -p ~/.config/fish/ && cat dotfiles/config.fish >> ~/.config/fish/config.fish

# Install homebrew if not already installed
if [[ $(which brew) ]]; then 
    echo "Homebrew Already Installed"
else 
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi;

# brew install things specified in the Brewfile
brew bundle --file=~/.brewfile

# source the bash profile, just for fun
source ~/.bash_profile

# auto setup conda
conda init zsh
conda init bash

### If you want to use fish shell, usually have to run this separately ###
# switch default shell to fish
sudo echo $(which fish) >> /etc/shells
chsh -s `which fish`

# automatically setup conda for fish
conda init fish

# install oh my fish
# now must choose omf theme
curl -L https://get.oh-my.fish | fish
