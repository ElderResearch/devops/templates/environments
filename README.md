# ERI Environment Setup

Scripts and utilities to standardize and automate setting up a laptop for work at ERI, organized by platform (Mac or PC).

## All environments

Please ensure that an _open source_ Java JDK version *8* is installed. We recommend Amazon Coretto:
https://docs.aws.amazon.com/corretto/latest/corretto-8-ug/downloads-list.html

## PC Setup

Use [ninite.com](https://ninite.com/) to download and install freely available software and utilities in batch, including Amazon Corretto.

## Mac Setup

The mac install script can be run from a shell as shown below. This script uses [Homebrew](https://brew.sh/) to install a bunch of common applications (both developer and non-developer). The list of applications can be reviewed in `mac/dotfiles/.brewfile`. Contributions encouraged!

```bash
git clone git@gitlab.com:ElderResearch/devops/env-setup.git && cd env-setup/mac
bash setup_install.sh
```
